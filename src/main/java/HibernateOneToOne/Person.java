package HibernateOneToOne;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity //Using Hibernate
@Data
@Builder
class Person {
    @Id             // This is primary key
    @GeneratedValue //Automatically increasing id
    private Long id;
    private String name;
}
