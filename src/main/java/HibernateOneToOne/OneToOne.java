package HibernateOneToOne;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class OneToOne implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(OneToOne.class,args);
    }
    //Sử dụng @RequiredArgsConstructure và final để thay cho @Autowired
    private final PersonRepository personRepository;
    private final AddressRepository addressRepository;
    @Override
    public void run(String... args) throws Exception{
        //create object Person
        Person person =Person.builder().name("hieu").build();
        //Save Person into data
        personRepository.save(person);
        //create object Address
        Address address =Address.builder()
                .city("đà lạt")
                .province("lâm đồng")
                .person(person)
                .build();
        //Save address into data
        addressRepository.save(address);
    }
}
