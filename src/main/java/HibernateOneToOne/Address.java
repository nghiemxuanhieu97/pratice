package HibernateOneToOne;


import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import javax.persistence.OneToOne;


@Entity //Using Hibernate
@Data
@Builder
public class Address {
    @Id
    @GeneratedValue
    private Long id;
    private String city;
    private String province;

    @OneToOne                       //Relationship 1-1
    @JoinColumn(name = "persion_id") //Connect each other by foreign key "person_id
    private Person person;
}
