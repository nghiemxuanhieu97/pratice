package lesson10RequestMappingModelAttribute;

import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Todo {
    String title;
    String detail;


    @Override
    public String toString(){
        return String.format("Todo[%s : %s]",title,detail);
    }

}
