package HibernateManyToMany;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.google.common.collect.Lists;

import javax.transaction.Transactional;
import java.util.Collections;


@SpringBootApplication
@RequiredArgsConstructor
public class ManyToMany implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(ManyToMany.class,args);
    }
    private final PersonRepository personRepository;
    private final AddressRepository addressRepository;
    @Override
    @Transactional
    public void run(String... args) throws Exception {
        Address address = Address.builder()
                .city("Sài Gòn")
                .build();
        Address address1 = Address.builder()
                .city("Đà Lạt")
                .build();

        // Tạo ra đối tượng person
        Person person1 = Person.builder()
                .name("Trang")
                .build();
        Person person2 = Person.builder()
                .name("Hiếu")
                .build();
//        address.setPersons(Lists.newArrayList(person1, person2));
//        address1.setPersons(Lists.newArrayList(person1));
        address.setPeople(Collections.singleton(person1));
        address.setPeople(Collections.singleton(person2));
        address1.setPeople(Collections.singleton(person1));

        addressRepository.saveAndFlush(address);
        addressRepository.saveAndFlush(address1);



    }
}
