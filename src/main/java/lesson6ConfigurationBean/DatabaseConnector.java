package lesson6ConfigurationBean;

public abstract class DatabaseConnector {
    //Đối tượng này sẽ làm nhiệm vụ kết nối tới một database bất kỳ
    private  String url;
    public abstract void connect();
    public String getUrl(){
        return url;
    }
    public void setUrl(String url){
        this.url = url;
    }
}
