package lesson6ConfigurationBean;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Lesson6 {


    public static void main(String[] args) {

        //Tìm các bean và đưa vào container context
        ApplicationContext context = SpringApplication.run(Lesson6.class);
//        //Lấy BeanSimple ra từ context
//        SimpleBean simpleBean = context.getBean(SimpleBean.class);
//        //In ra màn hình
//        System.out.println(simpleBean.toString());
        DatabaseConnector mysql = (DatabaseConnector) context.getBean("mySqlConnector");
        mysql.connect();



    }
}
