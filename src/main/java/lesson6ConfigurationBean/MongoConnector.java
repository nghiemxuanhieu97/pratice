package lesson6ConfigurationBean;



public class MongoConnector extends DatabaseConnector {
    @Override
    public void connect() {
        System.out.println("Connected to Mongo: "+getUrl());
    }
}
