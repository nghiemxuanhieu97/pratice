package lesson6ConfigurationBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Configuration
public class AppConfig {
    @Bean("mySqlConnector")
    DatabaseConnector mySqlConfig(){
        DatabaseConnector mySqlConnector = new MySqlConnector();
        mySqlConnector.setUrl("mysql url");
        return mySqlConnector;
    }
    @Bean("mongoConnector")
    DatabaseConnector mongoConfig(){
        DatabaseConnector mongoConnector = new MongoConnector();
        mongoConnector.setUrl("mongo url");
        return mongoConnector;
    }



}
