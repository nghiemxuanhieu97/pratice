package lesson9ThymeleafExpression;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class lesson9Controller {
    @GetMapping("/lesson9")
    public String indexWeek(@RequestParam(name="today",required = false,defaultValue = "") String day, Model model){
        model.addAttribute("today",day);
        return "lesson9";
    }

    @GetMapping("/profile")
    public String profile(Model model){
        List<Info> profile = new ArrayList<>();
//        profile.add(new Info("fullname", "Nguyễn Hoàng Nam"));
//        profile.add(new Info("nickname", "lốddaf"));
//        profile.add(new Info("gmail", "loda.namnh@gmail.com"));
//        profile.add(new Info("facebook", "https://www.facebook.com/nam.tehee"));
//        profile.add(new Info("website", "https://loda.me"));


        model.addAttribute("hieuprofile",profile);
        return "profile";
    }
}
