package ConnectionJDBC;
import com.microsoft.sqlserver.jdbc.*;
import lombok.var;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JDBC {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String url = "jdbc:sqlserver://localhost:1433;databaseName=northwind;username=sa;password=123";
        Connection conn = DriverManager.getConnection(url);
            if (conn != null) {
                System.out.println("Connected");
            }
            var sql = "select * from dbo.Categories";
            var statement = conn.prepareStatement(sql);
            var resultSet = statement.executeQuery();
            showResult(resultSet);
    }
    private static void showResult(ResultSet resultSet) throws SQLException {
        System.out.printf("%-6s%-20s%-20s\n","ID","Category Name","Description");
        while(resultSet.next()){
            var id = resultSet.getInt("CategoryID");
            var categoryName = resultSet.getString("CategoryName");
            var description = resultSet.getString("Description");
            System.out.printf("%-6s%-20s%-20s\n",id,categoryName,description);
        }
    }

}
