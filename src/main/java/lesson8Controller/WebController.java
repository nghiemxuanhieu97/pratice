package lesson8Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {
    //Nhận GET request
    @GetMapping("/")
    public String index()
    {
        return "index";//Trả về index.html
    }

    @GetMapping("/about")
    public String about(){
        return "about";
    }

    @GetMapping("/hello")
    public String hello(@RequestParam(value="name",required=false,defaultValue = "")String name, Model model){
        //Param: ?name=abc
        //@RequestParam sẽ lấy giá trị biến name gán vào tham số name
        //Model là một đối tượng của Spring Boot, được gắn vào cho mọi response
        //Nghĩa là: thông tin tìm được đưa hết vào model và spring boot sử dụng
        //Template Engine trong Thymeleaf để lấy thông tin này tạo thành html
        model.addAttribute("name",name);
        return "hello";
    }
}
