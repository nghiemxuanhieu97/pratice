package lesson8Controller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson8 {
    public static void main(String[] args) {
        SpringApplication.run(Lesson8.class,args);
    }

}
