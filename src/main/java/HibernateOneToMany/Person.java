package HibernateOneToMany;

import lombok.*;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name="address_id")
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private Address address;
}
