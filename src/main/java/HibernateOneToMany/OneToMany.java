package HibernateOneToMany;


import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collection;
import java.util.Collections;

@SpringBootApplication
@RequiredArgsConstructor
public class OneToMany implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(OneToMany.class, args);
    }
    private final AddressRepository addressRepository;
    @Override
    public void run(String... args) throws Exception {
        Address address = new Address();
        address.setCity("Đà Lạt");
        address.setProvince("Lâm Đồng");

        Person person = new Person();
        person.setName("Hiếu");
        person.setAddress(address);
        address.setPerons(Collections.singleton(person));
        addressRepository.saveAndFlush(address);
        Person person2 = new Person();
        person2.setName("Trang");
        person2.setAddress(address);
        address.setPerons(Collections.singleton(person2));
        addressRepository.saveAndFlush(address);
    }
}
