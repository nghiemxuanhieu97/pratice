package lesson7ConfigValue;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Value("${hieu}")
    String mySqlUrl;

    @Bean
    DatabaseConnector mysqlConfigure(){
        DatabaseConnector mySqlConnector = new MySqlConnector();
        System.out.println(mySqlUrl);
        mySqlConnector.setUrl(mySqlUrl);
        return mySqlConnector;
    }
}
