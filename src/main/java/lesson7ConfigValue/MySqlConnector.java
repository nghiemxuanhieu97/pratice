package lesson7ConfigValue;

public class MySqlConnector extends DatabaseConnector {
    @Override
    public void connect() {
        System.out.println("Connected to MySql: "+getUrl());
    }
}
