package lesson7ConfigValue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Lesson7 {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(Lesson7.class,args);
        DatabaseConnector data = context.getBean(DatabaseConnector.class);
        data.connect();
    }
}
