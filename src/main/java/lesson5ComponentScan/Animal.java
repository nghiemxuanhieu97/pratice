package lesson5ComponentScan;

import org.springframework.stereotype.Component;

@Component
public class Animal {
    @Override
    public String toString(){
        return "Animal";
    }
}
