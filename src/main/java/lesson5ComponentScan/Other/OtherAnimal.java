package lesson5ComponentScan.Other;

import org.springframework.stereotype.Component;

@Component
public class OtherAnimal {
    @Override
    public String toString(){
        return "Other Animal";
    }
}
