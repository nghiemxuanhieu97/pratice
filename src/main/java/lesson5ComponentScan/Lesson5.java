package lesson5ComponentScan;

import lesson5ComponentScan.Other.OtherAnimal;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = {"lesson5ComponentScan.Other"})
public class Lesson5 {
    public static void main(String[] args) {
        //Tìm kiếm hết các Bean được đánh dấu là component
        ApplicationContext context = SpringApplication.run(Lesson5.class,args);
        try{
            Animal animal = context.getBean(Animal.class);
            System.out.println("Animal found");
        }catch (Exception e){
            System.out.println("not find animal");
        }


        try{
            OtherAnimal otherAnimal = context.getBean(OtherAnimal.class);
            System.out.println("Other animal found");
        }catch (Exception e){
            System.out.println("not find other animal");
        }



    }

}
