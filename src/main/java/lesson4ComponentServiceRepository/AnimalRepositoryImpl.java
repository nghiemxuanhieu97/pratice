package lesson4ComponentServiceRepository;


import org.springframework.stereotype.Repository;
//@Repository này cũng là một @Component
@Repository
public class AnimalRepositoryImpl implements AnimalRepository {
    @Override
    public Animal getAnimalByName(String name) {
        //Database trả về một animal theo tên
        //Ở đây giả định là database: AnimalRepository trả về một animal có tên đúng với tên trong tham số
        //Thực tế ta phải query trong cơ sở dữ liệu
        return new Animal(name);
    }
}
