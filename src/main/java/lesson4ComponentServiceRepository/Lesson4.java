package lesson4ComponentServiceRepository;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
@SpringBootApplication
public class Lesson4 {
    public static void main(String[] args) {
        //Lấy tất cả các bean bỏ vào container context
        ApplicationContext context = SpringApplication.run(Lesson4.class,args);
        //Lấy bean AnimalService
        AnimalService animalService = context.getBean(AnimalService.class);
        //Lấy ra random một animal từ tầng service
        Animal animal = animalService.getRandomAnimal();
        System.out.println(animal);
    }
}
