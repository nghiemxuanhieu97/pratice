package lesson4ComponentServiceRepository;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//  @Service cũng là một @Component
@Service
public class AnimalService {
    @Autowired
    private AnimalRepository animalRepository;

    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    public Animal getRandomAnimal(){
        //Random một cái tên có độ dài là 10 ký tự
        String name = randomAnimalName(10);
        //Gọi xuống tầng repository để query lấy một animal với tên gì đó trong database
        return this.animalRepository.getAnimalByName(name);

    }
    public String randomAnimalName(int length){
        //Random một string có độ dài quy định
        //Sử dụng thư viện Apache Common Lang
        return RandomStringUtils.randomAlphabetic(length).toLowerCase();
    }
}
