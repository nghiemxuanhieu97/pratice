package lesson4ComponentServiceRepository;

//Interface này để giao tiếp với database
public interface AnimalRepository {
    //Tìm kiếm một animal theo tên @param name và trả về @return
    Animal getAnimalByName(String name);
}
